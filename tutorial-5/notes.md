Batasan :

- Mahasiswa tidak dapat dihapus jika telah menjadi asisten suatu mata kuliah
- Mata kuliah tidak dapat dihapus jika telah memiliki asisten 
- Mahasiswa dapat membuat lebih banyak log (n)
- Mahasiswa dapat mengupdate dan menghapus log
- Mahasiswa dapat melihat laporan pembayaran tiap bulan berupa data bulan,jam kerja,jumlah uang
- Sebuah mata kuliah dapat memiliki lebih dari 1 asisten
- Seorang mahasiswa hanya bisa menjadi asisten pada satu mata kuliah saja
- Relasi Mahasiswa dan Matakuliah adalah N-1
   
Relasi:

Mahasiswa
NPM: String (Primary Key)
Nama : String
Email: String
ipk: String
noTelp: String

MataKuliah
KodeMatkul: String (Primary Key)
namaMatkul: String
prodi: String

Log 
idLog: int (Primary Key, auto increment)
start: String
end: String
Deskripsi: String