package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_log")
    private int idLog;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "startTime")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "endTime")
    private  LocalDateTime endTime;

    @Column(name = "Deskripsi")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;

    public Log(int idLog, LocalDateTime startTime, LocalDateTime endTime, String deskripsi){
        this.idLog = idLog;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}
