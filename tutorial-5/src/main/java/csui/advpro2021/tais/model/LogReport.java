package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import java.text.DateFormatSymbols;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.time.Duration;

@Data
public class LogReport {
    private int month;
    private double jamKerja;
    private double Pembayaran;

    public static LogReport createLogReportMonthly(List<Log> logList,int bulan){

        double[] jam = new double[13];
        for(Log log : logList){
            jam[log.getStartTime().getMonthValue()] += Duration.between(log.getStartTime(),log.getEndTime()).toSeconds();
        }
        LogReport logReport = new LogReport();
        logReport.setMonth(bulan);
        logReport.setJamKerja(jam[bulan]/3600.0);
        logReport.setPembayaran(jam[bulan]/3600.0 * 350.0);
        return logReport;
    }

    public static Iterable<LogReport> createLogReport(List<Log> logList){
        List<LogReport> logReports = new ArrayList<>();
        double[] jam = new double[13];
        for(Log log : logList){
            jam[log.getStartTime().getMonthValue()] += Duration.between(log.getStartTime(),log.getEndTime()).toSeconds();
        }
        for(int i = 1; i < 13;i++){
            LogReport logReport = new LogReport();
            logReport.setMonth(i);
            logReport.setJamKerja(jam[i]/3600.0);
            logReport.setPembayaran(jam[i]/3600.0 * 350.0);
            logReports.add(logReport);
        }
        return logReports;
    }
}




