package csui.advpro2021.tais.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import csui.advpro2021.tais.model.Log;

public interface LogRepository extends JpaRepository<Log, Integer> {
    Log findByIdLog(int idLog);
}
