package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/{npm}/log", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@PathVariable(value = "npm") String npm,@RequestBody Log log) {
        Log log1 = logService.createLog(log,npm);
        return ResponseEntity.ok(log1);
    }

    @GetMapping(path ="/{npm}/getLog",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity <Iterable<Log>> getLog(@PathVariable(value = "npm") String npm){
        if(logService.getLog(npm) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getLog(npm));
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog,log));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/report/{npm}/{month}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLogReportMonthly(@PathVariable(value = "npm") String npm,@PathVariable(value = "month") int month) {
        return ResponseEntity.ok(logService.createLogReportMonthly(npm,month));
    }

    @GetMapping(path = "/report/{npm}/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity <Iterable<LogReport>> createLogReport(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.createLogReport(npm));
    }

}