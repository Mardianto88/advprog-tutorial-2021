package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    LogRepository logRepository;

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getLog(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return mahasiswa.getLogs();
    }

    @Override
    public void deleteLog(int idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public Log updateLog(int idLog, Log log){
        log.setMahasiswa(logRepository.findByIdLog(idLog).getMahasiswa());
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public LogReport createLogReportMonthly(String npm,int bulan){
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return LogReport.createLogReportMonthly(mahasiswa.getLogs(),bulan);
    }

    @Override
    public Iterable<LogReport> createLogReport(String npm){
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return LogReport.createLogReport(mahasiswa.getLogs());
    }
}
