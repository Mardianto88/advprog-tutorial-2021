package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if(mahasiswa.getMatkul().equals("")){
            mahasiswaRepository.deleteById(npm);
        }
    }

    @Override
    public Mahasiswa jadiAsdos(String npm,String kode_matkul){
        MataKuliah mataKuliah = mataKuliahRepository.findByKodeMatkul(kode_matkul);
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);

        if((mahasiswaRepository.findByNpm(npm).getMatkul()).equals("")){
            mahasiswa.setMataKuliah(mataKuliah);
            mataKuliah.getListAsdos().add(mahasiswa);
            mahasiswaRepository.save(mahasiswa);
            mataKuliahRepository.save(mataKuliah);
        }
        return mahasiswa;
    }
}

