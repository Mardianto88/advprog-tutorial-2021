package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(Log log, String npm);

    Iterable<Log> getLog(String npm);

    Log updateLog(int idlog,Log log);

    void deleteLog(int idLog);
    Iterable<LogReport> createLogReport(String npm);

    LogReport createLogReportMonthly(String npm,int bulan);
}
