package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("190765","caca","xxxx","3.7","12");
        log = new Log();
        log.setIdLog(1);
        log.setStartTime(LocalDateTime.of(2021,04,22,06,54));
        log.setEndTime(LocalDateTime.of(2021,04,22,07,54));
        log.setDeskripsi("desc");
    }

//    Log createLog(Log log, String npm);
//
//    Iterable<Log> getLog(String npm);
//
//    Log updateLog(int idlog,Log log);
//
//    void deleteLog(int idLog);
//    Iterable<LogReport> createLogReport(String npm);
//
//    LogReport createLogReportMonthly(String npm,int bulan);
//    @Test
//    public void testServiceCreateLog(){
//        lenient().when(logService.createLog(log,mahasiswa.getNpm())).thenReturn(log);
//    }

//    @Test
//    public void testServiceGetListLog(){
//        Iterable<Log> logs = logRepository.findAll();
//        lenient().when(logService.getLog(mahasiswa)).thenReturn(logs);
//    }
//    @Test
//    void testServiceCreateLog() {
//        lenient().when(logService.createLog(log,"12345890")).thenReturn(log);
//        Log loge = logService.createLog(log,"12345890");
//        Assertions.assertEquals(loge.getMahasiswa(),log.getMahasiswa());
//    }

}
