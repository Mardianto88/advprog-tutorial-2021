package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;
    private LogReport logReport;
    private Mahasiswa mahasiswa;
    private  MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("190765","caca","xxxx","3.7","12");
        mataKuliah = new MataKuliah("MD","MatDis","Ilkom");
        mahasiswa.setMataKuliah(mataKuliah);
        log = new Log(1,LocalDateTime.of(2021,04,22,06,54),LocalDateTime.of(2021,04,22,07,54),"desc");
        log.setDeskripsi("desc");
        log.setEndTime(LocalDateTime.of(2021,04,22,07,54));
        log.setStartTime(LocalDateTime.of(2021,04,22,06,54));
        log.setMahasiswa(mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListLog() throws Exception {
//        Iterable<Log> logList = Arrays.asList(log);
//        when(logService.getLog(mahasiswa.getNpm())).thenReturn(logList);
//
//        mvc.perform(get("/{npm}/getLog").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content();
    }

//
//    @Test
//    void testControllerGetLog() throws Exception {
//        when(logService.getLog(mahasiswa.getNpm())).thenReturn(mahasiswa.getLogs());
//        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.idLog").value(log.getIdLog()));
//    }

    @Test
    void testControllerUpdateLog() throws Exception {
        logService.createLog(log,mahasiswa.getNpm());

        int intLoglain = 22;
        log.setIdLog(intLoglain);

        when(logService.updateLog(intLoglain,log)).thenReturn(log);
    }

    @Test
    void testControllerDelete() throws Exception {
//        mataKuliahService.createMataKuliah(matkul);
//        mvc.perform(delete("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNoContent());
    }
}