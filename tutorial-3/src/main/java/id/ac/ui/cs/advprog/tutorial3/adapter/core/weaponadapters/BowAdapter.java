package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

public class BowAdapter implements Weapon {
    private Bow privateBow;
    private  boolean shot = false;

    public BowAdapter(Bow bow){
        this.privateBow = bow;
    }

    @Override
    public String normalAttack() {
        return privateBow.shootArrow(shot);
    }

    @Override
    public String chargedAttack() {
        shot = !(shot);
        if(shot){
            return "Entered Aim Shot Mode";
        } else {
            return "Leaving Aim Shot Mode";
        }
    }

    @Override
    public String getName() {
        return privateBow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return privateBow.getHolderName();
    }
}
