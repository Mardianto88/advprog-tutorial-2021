package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.*;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for(Bow bow : bowRepository.findAll()){
            if(weaponRepository.findByAlias(bow.getName()) == null){
                weaponRepository.save(new BowAdapter(bow));
            }
        }
        for(Spellbook spellbook : spellbookRepository.findAll()){
            if(weaponRepository.findByAlias(spellbook.getName()) == null){
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(weapon == null) {
            if(bowRepository.findByAlias(weaponName) != null){
                Bow bow = bowRepository.findByAlias(weaponName);
                weapon = new BowAdapter(bow);
            } else {
                Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
                weapon = new SpellbookAdapter(spellbook);
            }
        }
        weaponRepository.save(weapon);

        String log = weapon.getHolderName() + " attacked with " + weapon.getName();
        if(attackType == 0) {
            log += " (normal attack) : ";
            log += weapon.normalAttack();
        } else {
            log += " (charged attack) : ";
            log += weapon.chargedAttack();
        }
        logRepository.addLog(log);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}