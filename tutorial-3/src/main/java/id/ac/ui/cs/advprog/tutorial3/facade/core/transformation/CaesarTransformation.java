package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {
    private int key = 5;

    public CaesarTransformation(){}

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? -1 : 1;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char preChar = text.charAt(i);
            int newIdx = codex.getIndex(preChar) + key * selector;
            newIdx = newIdx < 0 ? codex.getCharSize() + newIdx : newIdx % codex.getCharSize();
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
