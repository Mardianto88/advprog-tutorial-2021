package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private int canUndo = 0;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        latestSpell = spells.get(spellName);
        latestSpell.cast();
        canUndo = 0;
    }

    public void undoSpell() {
        if(canUndo == 0 && latestSpell != null){
            latestSpell.undo();
            //number of undo = 1
            canUndo = 1;
        }

    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
