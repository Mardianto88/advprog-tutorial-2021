package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> arraySpell;
    public  ChainSpell(ArrayList<Spell> arraySpell){
        this.arraySpell = arraySpell;
    }

    @Override
    public void cast() {
        //cast every spell
        for(Spell x : arraySpell){
            x.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = arraySpell.size()-1; i >= 0;i--){
            // undo every spell reverse
            arraySpell.get(i).undo();

        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
