package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //set attack with magic and defend with shield for Mystic
    public MysticAdventurer(){
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    public String getAlias(){
        return "Mystic";
    }

}
