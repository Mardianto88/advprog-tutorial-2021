package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    //Update when request Delivery or Rumble
    @Override
    public void update(){
        String quest = this.guild.getQuestType();
        if(quest.equals("D") || quest.equals("R")){
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
