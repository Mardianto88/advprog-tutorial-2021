package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    public String defend() {
        return "Bertahan dengan barrier";
    }

    @Override
    public String getType(){
        return "DefendWithBarrier";
    }
}
