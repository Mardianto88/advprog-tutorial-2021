package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    public String attack() {
        return "Ciat! Ciat! Ciat!";
    }

    @Override
    public String getType(){
        return "AttackWithSword";
    }
}
