package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    //set attack with sword and defend with armor for Knight
    public KnightAdventurer(){
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias(){
        return "Knight";
    }
}
