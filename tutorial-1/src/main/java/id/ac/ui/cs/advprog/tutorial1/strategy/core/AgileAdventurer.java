package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {

    //set attack with gun and defend with barrier for Agile
    public AgileAdventurer(){
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias(){
        return "Agile";
    }
}
