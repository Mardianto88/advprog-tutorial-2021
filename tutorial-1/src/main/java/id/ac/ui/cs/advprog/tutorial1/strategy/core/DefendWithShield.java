package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    public String defend() {
        return "Bertahan dengan shield";
    }

    @Override
    public String getType(){
        return "DefendWithShield";
    }
}
